//
//  GameScene.m
//  Nunnies
//
//  Created by Ryan Salton on 10/09/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "GameScene.h"
#import "Nun.h"
#import "Bunny.h"

@implementation GameScene {
    NSInteger currentLevel;
    NSInteger maxNuns;
    NSInteger bunniesKilled;
    NSInteger nunsSaved;
    NSMutableArray *bunniesArray;
    
    NSArray *maxNunArray;
    NSArray *enemyFrequencyArray;
    NSArray *enemyVarianceArray;
}

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
        self.backgroundColor = [SKColor colorWithRed:0.2 green:0.15 blue:0.3 alpha:1.0];
        enemyFrequencyArray = @[@3.0, @2.7, @2.3, @1.8, @1.3, @0.8];
        enemyVarianceArray = @[@2, @2, @3, @4, @4, @4, @5];
        maxNunArray = @[@1, @4, @5, @6, @6, @7, @7, @8, @9];
        
        bunniesArray = [NSMutableArray array];
        currentLevel = 1;
        
        [self startLevel:currentLevel];
        
    }
    return self;
}

-(void)startLevel:(NSInteger)level
{
    [self showLevelLabel:level];
    [self enterTheNuns:level];
}

-(void)showLevelLabel:(NSInteger)level
{
    SKLabelNode *levelLabel = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-Thin"];
    levelLabel.text = [NSString stringWithFormat:@"Level %li", level];
    levelLabel.position = CGPointMake(self.size.width * 0.5, -30);
    [self addChild:levelLabel];
    [levelLabel runAction:[SKAction moveToY:self.size.height * 0.5 duration:0.2] completion:^{
        [levelLabel runAction:[SKAction sequence:@[[SKAction waitForDuration:0.5], [SKAction fadeAlphaTo:0 duration:0.2]]]];
    }];
}

-(void)enterTheNuns:(NSInteger)level
{
    NSInteger thisLevel = level;
    SKNode *enemyNode = [SKNode node];
    
    NSInteger direction = [self randomNumberBetweenMin:0 andMax:2];
    NSLog(@"%li", direction);
    CGPoint position = CGPointMake(direction == 1 ? -(ENEMY_SIZE * 0.5) : self.size.width + (ENEMY_SIZE * 0.5), self.size.height * 0.5);
    CGPoint destination = CGPointMake(direction != 1 ? -(ENEMY_SIZE * 0.5) : self.size.width + (ENEMY_SIZE * 0.5), self.size.height * 0.5);
    CGFloat speed = 0;
    
    if([self randomNumberBetweenMin:0 andMax:2] == 1){
        // GENERATE NUN
        NunType nunType = [self chooseNunType];
        Nun *nun = [[Nun alloc] initWithType:nunType andDirection:direction];
        nun.name = @"Nun";
        enemyNode.position = position;
        [enemyNode addChild:nun];
        speed = nun.travelSpeed;
    }
    else{
        // GENERATE BUNNY
        BunnyType bunnyType = [self chooseBunnyType];
        Bunny *bunny = [[Bunny alloc] initWithType:bunnyType andDirection:direction];
        bunny.name = @"Bunny";
        enemyNode.position = position;
        [enemyNode addChild:bunny];
        speed = bunny.travelSpeed;
        [bunniesArray addObject:enemyNode];
    }
    [self addChild:enemyNode];
    [enemyNode runAction:[SKAction moveTo:destination duration:speed] completion:^{
        if([bunniesArray containsObject:enemyNode]){
            [bunniesArray removeObject:enemyNode];
            [self gameOver];
        }
        else{
            nunsSaved++;
            if(nunsSaved == (currentLevel - 1 < maxNunArray.count ? [maxNunArray[currentLevel - 1] integerValue] : [maxNunArray[maxNunArray.count - 1] integerValue])){
                nunsSaved = 0;
                currentLevel++;
                [self startLevel:currentLevel];
            }
        }
        [enemyNode removeFromParent];
    }];
    
    [self runAction:[SKAction sequence:@[[SKAction waitForDuration:level < enemyFrequencyArray.count ? [enemyFrequencyArray[level] floatValue] : [enemyFrequencyArray[enemyFrequencyArray.count - 1] floatValue]], [SKAction customActionWithDuration:0 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
        if(thisLevel == currentLevel){
            [self enterTheNuns:currentLevel];
        }
    }]]]];
}

-(NunType)chooseNunType
{
    switch ([self randomNumberBetweenMin:0 andMax:currentLevel < enemyVarianceArray.count ? [enemyVarianceArray[currentLevel] floatValue] : [enemyVarianceArray[enemyVarianceArray.count - 1] floatValue]]) {
        case 1:
            return kNunWalking;
            break;
        case 2:
            return kNunRunning;
            break;
        case 3:
            return kNunPogo;
            break;
        case 4:
            return kNunCycle;
            break;
        default:
            break;
    }
    return kNunWalking;
}

-(BunnyType)chooseBunnyType
{
    switch ([self randomNumberBetweenMin:0 andMax:currentLevel < enemyVarianceArray.count ? [enemyVarianceArray[currentLevel] floatValue] : [enemyVarianceArray[enemyVarianceArray.count - 1] floatValue]]) {
        case 1:
            return kBunnyJumping;
            break;
        case 2:
            return kBunnyRunning;
            break;
        case 3:
            return kBunnyCycle;
            break;
        case 4:
            return kBunnyPogo;
            break;
        default:
            break;
    }
    return kBunnyJumping;
}

-(void)gameOver
{
    
}

-(NSInteger)randomNumberBetweenMin:(NSInteger)min andMax:(NSInteger)max
{
    return min + arc4random() % (max - min);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if ([node.name isEqualToString:@"Bunny"]){
        SKNode *killNode;
        for(SKNode *nodeView in bunniesArray){
            if([nodeView isEqual:node.parent]){
                killNode = nodeView;
                [nodeView removeFromParent];
            }
        }
        [killNode removeFromParent];
        bunniesKilled++;
    }
    else if ([node.name isEqualToString:@"Nun"]){
        [self gameOver];
    }
    
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

@end
