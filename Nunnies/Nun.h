//
//  Nun.h
//  Nunnies
//
//  Created by Ryan Salton on 10/09/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

typedef NS_ENUM(NSUInteger, NunType) {
    kNunWalking,
    kNunRunning,
    kNunCycle,
    kNunPogo
};

@interface Nun : SKSpriteNode

@property (nonatomic, assign) CGFloat travelSpeed;
-(id)initWithType:(NunType)nunType andDirection:(NSInteger)direction;

@end
