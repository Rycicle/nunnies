//
//  MenuScene.m
//  Nunnies
//
//  Created by Ryan Salton on 10/09/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "MenuScene.h"
#import "GameScene.h"

@implementation MenuScene

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
        self.backgroundColor = [SKColor colorWithRed:0.3 green:0.15 blue:0.3 alpha:1.0];
        
        SKSpriteNode *startButton = [SKSpriteNode spriteNodeWithColor:[SKColor redColor] size:CGSizeMake(180, 40)];
        startButton.position = CGPointMake(size.width * 0.5, size.height * 0.5);
        SKLabelNode *startLabel = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-Thin"];
        startLabel.text = @"Start";
        startLabel.position = CGPointMake(0, -startButton.size.height * 0.25);
        [startButton addChild:startLabel];
        startButton.name = @"StartButton";
        startLabel.name = @"StartButton";
        [self addChild:startButton];
        
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if([node.name isEqualToString:@"StartButton"]){
        SKScene * scene = [GameScene sceneWithSize:self.view.bounds.size];
        scene.scaleMode = SKSceneScaleModeAspectFill;
        [self.view presentScene:scene];
    }
    
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

@end
