//
//  Nun.m
//  Nunnies
//
//  Created by Ryan Salton on 10/09/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "Nun.h"

@implementation Nun

-(id)initWithType:(NunType)nunType andDirection:(NSInteger)direction
{
    self = [super init];
    if(self){
        
        self.size = CGSizeMake(ENEMY_SIZE, ENEMY_SIZE);
        self.color = [SKColor yellowColor];
        
        [self findSpeed:nunType];
        
    }
    return self;
}

-(void)findSpeed:(NunType)nunType
{
    switch (nunType) {
        case kNunWalking:
            self.travelSpeed = 7;
            break;
        case kNunRunning:
            self.travelSpeed = 3;
            break;
        case kNunCycle:
            self.travelSpeed = 2;
            break;
        case kNunPogo:
            self.travelSpeed = 5;
            break;
        default:
            break;
    }
}

@end
