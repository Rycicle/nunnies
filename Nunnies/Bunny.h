//
//  Bunny.h
//  Nunnies
//
//  Created by Ryan Salton on 10/09/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

typedef NS_ENUM(NSUInteger, BunnyType) {
    kBunnyJumping,
    kBunnyRunning,
    kBunnyCycle,
    kBunnyPogo
};

@interface Bunny : SKSpriteNode

@property (nonatomic, assign) CGFloat travelSpeed;
-(id)initWithType:(BunnyType)bunnyType andDirection:(NSInteger)direction;

@end
