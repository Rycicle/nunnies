//
//  Bunny.m
//  Nunnies
//
//  Created by Ryan Salton on 10/09/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "Bunny.h"

@implementation Bunny

-(id)initWithType:(BunnyType)bunnyType andDirection:(NSInteger)direction
{
    self = [super init];
    if(self){
        
        self.size = CGSizeMake(ENEMY_SIZE, ENEMY_SIZE);
        self.color = [SKColor redColor];
        
        if(bunnyType == kBunnyJumping){
            [self runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction moveByX:direction == 1 ? 20 : -20 y:50 duration:0.3], [SKAction moveByX:direction == 1 ? -20 : 20 y:-50 duration:0.2]]]]];
        }
        
        [self findSpeed:bunnyType];
        
    }
    return self;
}

-(void)findSpeed:(BunnyType)bunnyType
{
    switch (bunnyType) {
        case kBunnyJumping:
            self.travelSpeed = 5;
            break;
        case kBunnyRunning:
            self.travelSpeed = 3;
            break;
        case kBunnyCycle:
            self.travelSpeed = 2;
            break;
        case kBunnyPogo:
            self.travelSpeed = 6;
            break;
        default:
            break;
    }
}

@end
